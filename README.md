# philosopher-chatbot


## Getting started

This is a project created by coginitive science student and python developer Kamil Skowronek for a university AI an event that takes place in the department of philosophy and sociology. <br>

This project is using django web framework, machine learning and natural langage processing for three aplications:
- Philosopher Quiz (Quiz where user is trying to guess wich philosoher said a phrase or question)
- Philosophical Match (User have to answer 10 questions, then program will match him with a philosopher)
- Philosophy forever (User have a chance to speak with chatbot traind on philosophy data)



## Git 
-   first
```
cd existing_repo
git init
git remote add origin https://gitlab.com/K.Skylark/philosopher-chatbot.git
git branch -M main
<!-- git checkout <branch-name> -->
```

-   Clone
```
git clone https://gitlab.com/K.Skylark/philosopher-chatbot.git
```

-   Push
```
git push -uf origin main
```

-   Pull
```
git pull origin main
```


## Installation
Create your enviroment via Conda or Pip



- Pip:
    ```conda create --name <env_name> --file requirements.txt```

## Usage
- Django:
    - start server ``` python3 manage.py runserver ```
    - migrations ``` python3 manage.py makemigrations, python3 manage.py migrate  ```
    To log into admin panel remeber to create superuser
    - ``` python3 manage.py createsuperuser ```


## Contact
- Me:
    - https://gitlab.com/K.Skylark
    - https://www.linkedin.com/in/kamil-skowronek-910204185/

## Special thanks to Kourosh Alizadeh for dataset!
- Link to dataset:
    - https://www.kaggle.com/datasets/kouroshalizadeh/history-of-philosophy
- Link to the project:
    - http://philosophydata.com/