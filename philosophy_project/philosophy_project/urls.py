"""philosophy_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from quiz.views import *
from philosophy.views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index_view, name='index'),
    path('quiz/', quiz, name='quiz'),
    path('philosophies/', philosophy_list, name='philosophy-list'),
    path('philosophies/<int:pk>/', philosophy_detail, name='philosophy-detail'),
    path('philosophies/create/', philosophy_create, name='philosophy-create'),
    path('philosophies/<int:pk>/update/', philosophy_update, name='philosophy-update'),
    path('philosophies/<int:pk>/delete/', philosophy_delete, name='philosophy-delete'),
    path('upload-csv/', upload_csv, name='upload-csv'),
    path('export-csv/', export_to_csv, name='export-csv'),

    path('count-word-by-author/', count_word_by_author, name='count_word_by_author'),
    path('count-authors/', count_authors, name='count_authors'),
    path('find-author-by-sentence/', find_author_by_sentence, name='find_author_by_sentence'),
    path('author-by-sentence-choice/', author_by_sentence_choice, name='author_by_sentence_choice'),
    path('play-game/', game, name='game'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

