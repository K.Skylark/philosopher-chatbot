import csv
import os
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import api_view
from .models import Philosophy
from .serializers import PhilosophySerializer
from rest_framework.exceptions import ValidationError
from datetime import datetime
from django.shortcuts import render
from .models import Philosophy
import matplotlib.pyplot as plt
import json
import difflib
def validate_philosophy_data(row):
    # Sprawdź, czy pole "title" nie jest puste
    if not row.get('title'):
        raise ValidationError("Field 'title' cannot be empty.")

    # Sprawdź, czy pole "sentence_length" jest liczbą
    sentence_length = row.get('sentence_length')
    if sentence_length is not None:
        try:
            int(sentence_length)
        except ValueError:
            raise ValidationError("Field 'sentence_length' must be a number.")

    try:
        # Waliduj pola, które wymagają liczby
        numeric_fields = ['original_publication_date', 'sentence_length']
        for field in numeric_fields:
            if row[field] != '':
                try:
                    int(row[field])
                except ValueError:
                    raise ValidationError(f"Invalid value '{row[field]}' for field '{field}'. Must be a number.")

        return True
    except ValidationError as e:
        print(f"Invalid data: {e}")
        return False
    


@csrf_exempt
@api_view(['POST'])
def upload_csv(request):
    file = request.FILES['file']
    decoded_file = file.read().decode('utf-8').splitlines()
    reader = csv.DictReader(decoded_file)

    for row in reader:
        try:
            # Waliduj dane przed zapisaniem
            if validate_philosophy_data(row):
                print(row)
                Philosophy.objects.create(
                    title=row['title'],
                    author=row['author'],
                    school=row['school'],
                    sentence_spacy=row['sentence_spacy'],
                    sentence_str=row['sentence_str'],
                    original_publication_date=int(row['original_publication_date']) if row['original_publication_date'] != '' else None,
                    corpus_edition_date=row['corpus_edition_date'],
                    sentence_length=int(row['sentence_length']) if row['sentence_length'] != '' else None,
                    sentence_lowered=row['sentence_lowered'],
                    tokenized_txt=row['tokenized_txt']
                )
        except ValidationError as e:
            print(f"Invalid data: {e}")
            continue

    return JsonResponse({'message': 'Data uploaded successfully'}, status=200)


import os
import csv
from datetime import datetime
from django.http import HttpResponse

def export_to_csv(request):
    # Pobierz aktualną datę i czas
    current_datetime = datetime.now()
    timestamp = current_datetime.strftime('%Y%m%d%H%M%S')  # Formatowanie daty i czasu jako łańcucha znaków

    # Utwórz nazwę pliku na podstawie daty i czasu
    file_name = f'philosophy_data_{timestamp}.csv'

    # Pobierz wszystkie rekordy z modelu Philosophy
    philosophies = Philosophy.objects.all()

    # Utwórz pełną ścieżkę do pliku
    file_path = os.path.join('../data/dowolands', file_name)

    # Otwórz plik CSV do zapisu
    with open(file_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)

        # Zapisz nagłówki kolumn
        headers = ['title', 'author', 'school', 'sentence_spacy', 'sentence_str', 'original_publication_date', 'corpus_edition_date', 'sentence_length', 'sentence_lowered', 'tokenized_txt']
        writer.writerow(headers)

        # Zapisz rekordy do pliku CSV
        for philosophy in philosophies:
            row = [philosophy.title, philosophy.author, philosophy.school, philosophy.sentence_spacy, philosophy.sentence_str, philosophy.original_publication_date, philosophy.corpus_edition_date, philosophy.sentence_length, philosophy.sentence_lowered, philosophy.tokenized_txt]
            writer.writerow(row)

    return JsonResponse({'message': f'Data exported to {file_path} successfully'}, status=200)



@api_view(['GET'])
def philosophy_list(request):
    philosophies = Philosophy.objects.all()
    serializer = PhilosophySerializer(philosophies, many=True)
    return JsonResponse(serializer.data, safe=False)

@api_view(['GET'])
def philosophy_detail(request, pk):
    philosophy = Philosophy.objects.get(pk=pk)
    serializer = PhilosophySerializer(philosophy)
    return JsonResponse(serializer.data)

@api_view(['POST'])
def philosophy_create(request):
    serializer = PhilosophySerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return JsonResponse(serializer.data, status=201)
    return JsonResponse(serializer.errors, status=400)

@api_view(['PUT'])
def philosophy_update(request, pk):
    philosophy = Philosophy.objects.get(pk=pk)
    serializer = PhilosophySerializer(instance=philosophy, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return JsonResponse(serializer.data)
    return JsonResponse(serializer.errors, status=400)

@api_view(['DELETE'])
def philosophy_delete(request, pk):
    philosophy = Philosophy.objects.get(pk=pk)
    philosophy.delete()
    return JsonResponse({'message': 'Philosophy deleted successfully'}, status=204)


from django.shortcuts import render
from django.http import HttpResponse

def count_word_by_author(request):
    if request.method == 'POST':
        word = request.POST.get('word', '')

        philosophies = Philosophy.objects.all()
        author_word_counts = {}

        for philosophy in philosophies:
            author = philosophy.author
            tokenized_txt = philosophy.tokenized_txt

            if author not in author_word_counts:
                author_word_counts[author] = 0

            if tokenized_txt:
                words = tokenized_txt
                if word in words:
                    author_word_counts[author] += 1

        authors = list(author_word_counts.keys())
        counts = list(author_word_counts.values())

        context = {
            'word': word,
            'authors': json.dumps(authors),
            'counts': json.dumps(counts)
        }

        return render(request, 'philosophy/count_word_by_author.html', context)

    return render(request, 'philosophy/count_word_by_author.html')


def count_authors(request):
    philosophies = Philosophy.objects.all()
    authors = set()

    for philosophy in philosophies:
        author = philosophy.author
        authors.add(author)

    context = {
        'authors': authors
    }

    return render(request, 'philosophy/count_authors.html', context)


import difflib

def find_author_by_sentence(request):
    if request.method == 'POST':
        sentence = request.POST.get('sentence')
        context = {}

        try:
            philosophy = Philosophy.objects.get(sentence_str=sentence)
            context['author'] = philosophy.author
        except Philosophy.DoesNotExist:
            philosophies = Philosophy.objects.all()
            closest_sentence, closest_author = find_closest_sentence(sentence, philosophies)
            if closest_sentence:
                context['closest_sentence'] = closest_sentence
                context['sentence'] = sentence
                context['closest_author'] = closest_author
                return render(request, 'philosophy/find_author_by_sentence_choice.html', context)
            else:
                context['sentence'] = sentence

        return render(request, 'philosophy/find_author_by_sentence.html', context)

    return render(request, 'philosophy/find_author_by_sentence_form.html')

def find_closest_sentence(sentence, philosophies):
    closest_ratio = 0
    closest_sentence = None
    closest_author = None

    for philosophy in philosophies:
        similarity_ratio = difflib.SequenceMatcher(None, sentence, philosophy.sentence_str).ratio()
        if similarity_ratio > closest_ratio:
            closest_ratio = similarity_ratio
            closest_sentence = philosophy.sentence_str
            closest_author = philosophy.author

    return closest_sentence, closest_author

def author_by_sentence_choice(request):
    if request.method == 'POST':
        choice = request.POST.get('choice')
        sentence = request.POST.get('sentence')

        if choice == 'yes':
            closest_author = request.POST.get('closest_author')
            if closest_author:
                context = {
                    'author': closest_author
                }
                return render(request, 'philosophy/find_author_by_sentence.html', context)
            else:
                try:
                    if sentence:
                        philosophy = Philosophy.objects.get(sentence_str=sentence)
                        context = {
                            'author': philosophy.author
                        }
                        return render(request, 'philosophy/find_author_by_sentence.html', context)
                    else:
                        context = {
                            'error_message': 'No suggested sentence available.'
                        }
                        return render(request, 'philosophy/find_author_by_sentence_error.html', context)
                except Philosophy.DoesNotExist:
                    context = {
                        'error_message': 'The suggested sentence does not exist.'
                    }
                    return render(request, 'philosophy/find_author_by_sentence_error.html', context)
        elif choice == 'no':
            return render(request, 'philosophy/find_author_by_sentence_form.html')

    return render(request, 'philosophy/find_author_by_sentence_form.html')




def game(request):
    questions = [
        {"question": "What is the significance of consciousness in experience?", "authors": ["Husserl", "Heidegger"]},
        {"question": "Does absolute truth exist?", "authors": ["Nietzsche"]},
        {"question": "Does the mind exist independently of the body?", "authors": ["Descartes", "Leibniz"]},
        {"question": "What is the significance of individual freedom?", "authors": ["Hegel", "Spinoza"]},
        {"question": "What is the nature of perception and knowledge?", "authors": ["Berkeley", "Hume"]},
        {"question": "What are the principles of ethics?", "authors": ["Kant"]},
        {"question": "What is the meaning of language in knowledge?", "authors": ["Wittgenstein", "Russell"]},
        {"question": "What are the fundamental characteristics of being?", "authors": ["Aristotle"]},
        {"question": "What is the nature of the soul?", "authors": ["Plato", "Socrates"]},
        {"question": "Does objective truth exist in the social sciences?", "authors": ["Foucault"]},
        {"question": "How to achieve inner peace of mind?", "authors": ["Epictetus"]},
        {"question": "What are the foundations of a political society?", "authors": ["Locke"]},
        {"question": "What are the fundamental class relations?", "authors": ["Marx"]},
        {"question": "How can we falsify scientific theories?", "authors": ["Popper"]},
    ]

    if request.method == 'POST':
        answers = {}
        scores = {}

        for i in range(len(questions)):
            answer = request.POST.get(f'answer{i+1}')
            answers[i] = answer

            selected_authors = questions[i]["authors"]
            philosophies = Philosophy.objects.filter(author__in=selected_authors)
            sentence_str_list = [philosophy.sentence_str for philosophy in philosophies]
            author_list = [philosophy.author for philosophy in philosophies]

            for j, sentence_str in enumerate(sentence_str_list):
                similarity = difflib.SequenceMatcher(None, answer, sentence_str).ratio()
                author = author_list[j]

                if author in scores:
                    scores[author] += similarity
                else:
                    scores[author] = similarity

        sorted_scores = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        winner = sorted_scores[0][0]
        winner_scores = {author: (score / sorted_scores[0][1]) * 100 for author, score in sorted_scores}

        context = {
            'questions': questions,
            'answers': answers,
            'winner': winner,
            'winner_scores': winner_scores,
        }
        
        return render(request, 'philosophy/game_result.html', context)

    context = {
        'questions': [question['question'] for question in questions],
    }
    return render(request, 'philosophy/game.html', context)
