from django import forms

class GameForm(forms.Form):
    question1 = forms.CharField(label='Pytanie 1')
    question2 = forms.CharField(label='Pytanie 2')
    # Kontynuuj dla pozostałych pytań...
    question10 = forms.CharField(label='Pytanie 10')
