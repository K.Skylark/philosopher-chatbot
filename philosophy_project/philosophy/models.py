from django.db import models

# Create your models here.

# Model based on philosophy_data dataset
class Philosophy(models.Model):
    title = models.CharField(max_length=125)
    author = models.CharField(max_length=50)
    school = models.CharField(max_length=50)
    sentence_spacy = models.TextField()
    sentence_str = models.TextField()
    original_publication_date = models.IntegerField() #year as int
    corpus_edition_date = models.IntegerField() #year as int
    sentence_length = models.IntegerField()
    sentence_lowered = models.TextField()
    tokenized_txt = models.TextField()
    lemmatized_str = models.TextField()
