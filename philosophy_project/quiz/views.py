import random
from django.shortcuts import render
from .models import User
from philosophy.models import Philosophy

def index_view(request):
    return render(request, 'quiz/index.html')


def quiz(request):
    if request.method == 'POST':
        name = request.POST.get('name')

        user = name
        user_score = 0

        if 'user' in request.session:
            user = request.session['user']
            user_score = request.session['user_score']

        if user is None:
            user = "Anonymous Gal"

        round = request.session.get('round', 1)

        if round == 1 and user is not None:
            user_score = 0

        scores = []

        if round <= 10:
            philosophy_ids = Philosophy.objects.values_list('id', flat=True)
            question_id = random.choice(philosophy_ids)
            question = Philosophy.objects.get(id=question_id).sentence_str
            right_answer = Philosophy.objects.get(id=question_id).author

            answers = [right_answer]

            while len(answers) < 4:
                random_id = random.choice(philosophy_ids)
                random_answer = Philosophy.objects.get(id=random_id).author

                if random_answer != right_answer and random_answer not in answers:
                    answers.append(random_answer)

            random.shuffle(answers)

            context = {
                'name': user,
                'round': round,
                'question': question,
                'answers': answers,
            }

            if request.method == 'POST':
                user_answer = request.POST.get('answer')
                if user_answer and user_answer.isdigit() and int(user_answer) in range(1, 5):

                    chosen_answer_index = int(user_answer) - 1

                    if answers[chosen_answer_index] == right_answer:
                        user_score += 1
                        context['Right answer!'] = True
                    else:
                        context['Wrong answer!'] = True
                else:
                    context['Incorrect choice'] = True

            request.session['round'] = round + 1
            request.session['user'] = user
            request.session['user_score'] = user_score

            return render(request, 'quiz/question.html', context)
        else:
            scores.append((user, user_score))
            request.session.pop('round', None)
            request.session.pop('user', None)
            request.session.pop('user_score', None)

            # Save user and score into database
            user = User.objects.create(name=user, score=user_score)
            user.save()

            database_scores = User.objects.all()

            context = {
                'name': user,
                'scores': database_scores,
            }
            return render(request, 'quiz/score.html', context)

    return render(request, 'quiz/quiz.html')
